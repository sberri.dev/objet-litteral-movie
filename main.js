// Création de l'objet "movie" pour le film X-Men
const movie = {

    movieTitle: "X Men", // Le titre du film
    yearOfCompletion: "2000", // l'année de la réalisation
    director: "James Mangold", // Le nom du réalisateur
    mainActors: ["Hugh Jackman", "James McAvoy", "Patrick Steward"], //  Les acteurs principaux du film
    favoriteMovie: true, // Si le film est un film favoris
    ratingOfFilm: "8,5", // Note du film (avec virgule)
    duration: 0, // Initialisation de la propriété "duration" à 0

    // Méthode pour définir la durée en minutes
    setDuration: function (hours, minutes) {

        this.duration = hours * 60 + minutes;  // Convertir les heures en minutes et ajouter les minutes
    }
}

// Utilisation de la méthode "setDuration" pour définir la durée du film
movie.setDuration(2, 17); // la durée du filme : 2 heures et 17 minutes

console.log(movie.duration);